import 'package:flutter/material.dart';
import 'package:flutter_contacts/flutter_contacts.dart';

class View extends StatefulWidget {
  const View({Key? key}) : super(key: key);

  @override
  State<View> createState() => _ViewState();
}

class _ViewState extends State<View> {
  @override
  Widget build(BuildContext context) {
    final Contact contacto =
        ModalRoute.of(context)!.settings.arguments as Contact;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Contactos'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Stack(
              alignment: Alignment.bottomLeft,
              children: [
                (contacto.photo == null)
                    ? Image.network(
                        'https://uning.es/wp-content/uploads/2016/08/ef3-placeholder-image.jpg')
                    : Image(image: MemoryImage(contacto.photo!)),
                Padding(
                  padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                  child: (contacto.displayName.isEmpty)
                      ? const Text('Contacto desconocido',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 30))
                      : Text(
                          '${contacto.name.first} ${contacto.name.last}',
                          style: const TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 30),
                        ),
                ),
              ],
            ),
            (contacto.phones.isEmpty)
                ? const Card(
                    child: Text(
                    'Añadir numero',
                    style: TextStyle(fontSize: 18),
                  ))
                : Card(
                    child: Row(
                      children: [
                        Row(
                          children: [
                            Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16),
                                  child: (contacto.phones.isNotEmpty)
                                      ? Text(
                                          contacto
                                              .phones.first.normalizedNumber,
                                          style: const TextStyle(fontSize: 18))
                                      : const Text(
                                          '--',
                                          style: TextStyle(fontSize: 18),
                                        ),
                                ),
                                const Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 16),
                                  child: Text(
                                    'Celular',
                                    style: TextStyle(
                                        fontSize: 14, color: Colors.grey),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: const [
                              Padding(
                                padding: EdgeInsets.all(16.0),
                                child: Icon(Icons.phone),
                              ),
                              Icon(Icons.message),
                              Padding(
                                padding: EdgeInsets.all(16.0),
                                child: Icon(Icons.whatsapp),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
            (contacto.emails.isEmpty)
                ? const Card(
                    child: Text(
                    'Añadir correo',
                    style: TextStyle(fontSize: 18),
                  ))
                : Card(
                    child: Row(
                      children: [
                        Row(
                          children: [
                            Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16),
                                  child: (contacto.emails.isNotEmpty)
                                      ? Text(
                                          contacto.emails.first.address,
                                          style: const TextStyle(fontSize: 16),
                                        )
                                      : const Text(
                                          '',
                                          style: TextStyle(fontSize: 16),
                                        ),
                                ),
                                const Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 16),
                                  child: Text(
                                    'Escuela',
                                    style: TextStyle(
                                        fontSize: 14, color: Colors.grey),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: const [
                              Padding(
                                padding: EdgeInsets.all(16.0),
                                child: Icon(Icons.mail),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
            Card(
              child: Row(
                children: [
                  Row(
                    children: [
                      Column(
                        children: const [
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 16),
                            child: Text(
                              '27 de Diciembre',
                              style: TextStyle(fontSize: 18),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 16),
                            child: Text(
                              'Cumpleaños',
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: const [
                        Padding(
                          padding: EdgeInsets.all(16.0),
                          child: Icon(Icons.calendar_month),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (() => {}),
        tooltip: 'Agregar Contacto',
        child: const Icon(Icons.add),
      ),
    );
  }
}
