import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_contacts/contact.dart';
import 'package:flutter_contacts/flutter_contacts.dart';
import 'package:url_launcher/url_launcher.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Contact>? contacts;
  @override
  void initState() {
    super.initState();
    getContacts();
  }

  void getContacts() async {
    if (await FlutterContacts.requestPermission()) {
      contacts = await FlutterContacts.getContacts(
          withProperties: true, withPhoto: true);
      print(contacts);
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Contactos'),
        actions: [
          IconButton(
            icon: const Icon(Icons.search),
            onPressed: () {},
            tooltip: "Buscar Contacto",
          ),
        ],
      ),
      body: (contacts) == null
          ? const Center(child: CircularProgressIndicator())
          : ListView.builder(
              itemCount: contacts!.length,
              itemBuilder: (BuildContext context, int index) {
                Uint8List? image = (contacts![index].photo);
                String num = (contacts![index].phones.isNotEmpty)
                    ? (contacts![index].phones.first.number)
                    : '--';
                Contact contacto = contacts![index];
                return Card(
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, '/view',
                        arguments: contacto);
                    },
                    splashColor: Colors.teal[30],
                    child: ListTile(
                      leading: (contacts![index].photo == null)
                          ? const CircleAvatar(
                              radius: 28,
                              backgroundColor: Colors.deepOrange,
                              child: Text(
                                'IP',
                                style: TextStyle(fontSize: 28),
                              ),
                            )
                          : CircleAvatar(
                              backgroundImage: MemoryImage(image!),
                              radius: 28,
                              backgroundColor: Colors.deepOrange,
                            ),
                      title: Text(
                          '${contacts![index].name.first} ${contacts![index].name.last}'),
                      subtitle: Text(num),
                    ),
                  ),
                );
              },
            ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          //Navigator.pop(context);
          Navigator.pushNamed(context, '/create');
        },
        tooltip: 'Agregar Contacto',
        child: const Icon(Icons.add),
      ),
    );
  }
}
