import 'package:flutter/material.dart';
import 'package:contactos/screens/home.dart';
import 'package:contactos/screens/create.dart';
import 'package:contactos/screens/view.dart';



void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Contactos',
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      home: const Home(),
      routes: {
        '/home': (context) => const Home(),
        '/create': (context) => const Create(),
        '/view': (context) => const View(),
      },
    );
  }
}
